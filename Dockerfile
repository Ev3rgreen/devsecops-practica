FROM docker.io/library/php:8-apache

LABEL org.opencontainers.image.source=https://github.com/digininja/DVWA
LABEL org.opencontainers.image.description="DVWA pre-built image."
LABEL org.opencontainers.image.licenses="gpl-3.0"
LABEL maintainer="Timo Pagel <dependencycheckmaintainer@timo-pagel.de>"

# Definir variables de entorno
ENV user=dependencycheck
ENV version_url=https://jeremylong.github.io/DependencyCheck/current.txt
ENV download_url=https://dl.bintray.com/jeremy-long/owasp

WORKDIR /var/www/html

# Instalar dependencias y configurar el entorno
RUN apt-get update \
 && export DEBIAN_FRONTEND=noninteractive \
 && apt-get install -y zlib1g-dev libpng-dev libjpeg-dev libfreetype6-dev iputils-ping wget ruby mono-runtime unzip \
 && apt-get clean -y && rm -rf /var/lib/apt/lists/* \
 && docker-php-ext-configure gd --with-jpeg --with-freetype \
 && docker-php-ext-install gd mysqli pdo pdo_mysql \
 && gem install bundle-audit \
 && gem cleanup \
 && wget -O /tmp/current.txt ${version_url} \
 && version=$(cat /tmp/current.txt) \
 && file="dependency-check-${version}-release.zip" \
 && wget "$download_url/$file" \
 && unzip ${file} \
 && rm ${file} \
 && mv dependency-check /usr/share/ \
 && useradd -ms /bin/bash ${user} \
 && chown -R ${user}:${user} /usr/share/dependency-check \
 && mkdir /report \
 && chown -R ${user}:${user} /report \
 && apt-get remove --purge -y wget \
 && apt-get autoremove -y \
 && rm -rf /var/lib/apt/lists/* /tmp/*

# Copiar archivos del proyecto
COPY --chown=www-data:www-data . .
COPY --chown=www-data:www-data config/config.inc.php.dist config/config.inc.php

# Configurar volúmenes
VOLUME ["/src", "/usr/share/dependency-check/data", "/report"]

# Definir usuario para las operaciones posteriores
USER ${user}

# Configurar el punto de entrada y el comando por defecto
WORKDIR /src
ENTRYPOINT ["/usr/share/dependency-check/bin/dependency-check.sh"]
CMD ["--help"]
